﻿using System;

namespace VaccinationScheduling
{
    class Program
    {
        static void Main(string[] args)
        {
            bool logging = false;

            Console.WriteLine("Do you want to work with the offline or online algorithm?");
            Console.WriteLine("enter 'on' for the online alg and 'off' for the offline algorithm");
            string setting = Console.ReadLine();

            while (setting != "on" && setting != "off")
            {
                Console.WriteLine("We did not receive on or off, try again: on / off");
                setting = Console.ReadLine();
            }

            if (setting == "on")
            {
                Online onlineProgram = new Online(logging, false);
            }
            else
            {
                Offline offlineProgram = new Offline(logging);
            }

            Console.ReadKey();
        }
    }
}
