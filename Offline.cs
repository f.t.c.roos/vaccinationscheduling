﻿using System;
using System.Diagnostics;
using System.IO;

//using Google.OrTools.LinearSolver;
using Google.OrTools.Sat;

namespace VaccinationScheduling
{
    struct Patient
    {
        public Patient(int first, int last, int delay, int interval)
        {
            First = first;
            Last = last;
            Delay = delay;
            Interval = interval;
        }

        // the first available time slot of the first dose
        public int First;
        // the last available time slot of the first dose
        public int Last;
        // the patient-dependent delay between the first and the second doses
        public int Delay;
        // the length of the second dose feasible interval
        public int Interval;
    }

    class Offline
    {
        Patient[] patients;

        // processingTimeOfFirstDose
        int p1;
        // processingTimeOfSecondDose
        int p2;
        // minimum gap between the first and the second dose
        int gap;
        // number of patients
        int amount;
        // the solver object
        CpSolver solver;
        // stopwatch to store the progress
        Stopwatch sw = new Stopwatch();
        // whether to print the log, printing is too expensive when calculating the runtime
        bool logging;
        // filename
        //string filename = "..\\..\\..\\inputOfflineOne.txt";
        //string filename = "..\\..\\..\\inputOfflineExample.txt";
        //string filename = "..\\..\\..\\inputOfflineTestInstance.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\20.txt";
        string filename = "..\\..\\..\\extra_offline_testcases\\25.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\30.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\35.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\40.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\45.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\50.txt";
        //string filename = "..\\..\\..\\extra_offline_testcases\\100.txt";

        public Offline(bool loggingToConsole)
        {
            logging = loggingToConsole;

            Console.WriteLine("Offline program");
            ReadingFromFile();

            int AmountOfRuns = 1;
            // we will take the average of 10 runtimes
            for (int i = 0; i < AmountOfRuns; i++)
            {
                Scheduling();
            }
            long average = sw.ElapsedMilliseconds / AmountOfRuns;
            WritingToFile("average time: " + average.ToString() + ", hospitals: " + solver.ObjectiveValue);

            Console.WriteLine("The end");
        }

        public void Scheduling()
        {
            int amountOfJabs = amount + amount;
            var model = new CpModel();

            // Start time of the in total 2n jabs
            IntVar[] StartTimeOfJab = new IntVar[amountOfJabs];

            // End time of the in total 2n jabs
            IntVar[] EndTimeOfJab = new IntVar[amountOfJabs];

            // Determine amount of hospitals needed
            int hospitalAmount = amount;

            // To much patients so decide an upperbound on the amount of hospitals
            if (amount >= 25)
            {
                // Use the online solver to find an upperbound on the amount of hospitals
                Online onlineSolver = new Online(false, true);
                onlineSolver.ReadingFromArray(p1, p2, gap, patients);
                hospitalAmount = (int)onlineSolver.outputMachines();
                if (logging)
                {
                    Console.WriteLine("Found upperbound on hospitals: " + hospitalAmount);
                }
            }

            // [i] 1 if hospital i is used; Length worst case n or an upperbound 
            IntVar[] Hospitals = new IntVar[hospitalAmount];

            // [i, j] 1 if hospital i gives Jab j, 0 otherwise; length [min(n, upperbound on hospitals), 2n]
            IntVar[,] JabInHospital = new IntVar[hospitalAmount, amountOfJabs];

            // [j, k] 1 if Jab j and Jab k overlap, 0 otherwise; length [2n, 2n]
            IntVar[,] JabsThatOverlap = new IntVar[amountOfJabs, amountOfJabs];

            //Determine latest possible finish time of a jab
            int maxTime = 0;
            for (int i = 0; i < amount; i++)
            {
                maxTime = Math.Max(maxTime, patients[i].Last - p1 + 1 + gap + patients[i].Delay + patients[i].Interval);
            }

            //Initialize variables for first Jab
            for (int j = 0; j < amount; j++)
            {
                StartTimeOfJab[j] = model.NewIntVar(patients[j].First, patients[j].Last - (p1 - 1) , "StartTimeOfJab_" + j);
                EndTimeOfJab[j] = model.NewIntVar(patients[j].First + (p1 - 1), patients[j].Last, "EndTimeOfJab_" + j);
            }

            //Initialize second jab
            for (int j = amount; j < amountOfJabs; j++)
            {
                int patientId = j - amount;
                //Start time of second jab is within the minimum time of the interval and the maximum time
                StartTimeOfJab[j] = model.NewIntVar(patients[patientId].First + (p1 - 1) + gap + patients[patientId].Delay,
                    patients[patientId].Last + gap + patients[patientId].Delay + patients[patientId].Interval - (p2 - 1), "StartTimeOfJab_" + j);

                //End time of second jab is within minimum time of interval and maximum time
                EndTimeOfJab[j] = model.NewIntVar(patients[patientId].First + (p1 - 1) + gap + patients[patientId].Delay + (p2 - 1),
                    patients[patientId].Last + gap + patients[patientId].Delay + patients[patientId].Interval, "EndTimeOfJab_" + j);
            }

            //Initialize Hospitals
            for (int i = 0; i < hospitalAmount; i++)
            {
                Hospitals[i] = model.NewBoolVar("Hospital_" + i);
            }

            //Initialize JabInHospital
            for (int i = 0; i < hospitalAmount; i++)
            {
                for (int j = 0; j < amountOfJabs; j++)
                {
                    JabInHospital[i, j] = model.NewBoolVar("JabInHospital_" + i + "_" + j);
                }
            }

            //Initialize JabsThatOverlap
            for (int j = 0; j < amountOfJabs; j++)
            {
                for (int k = 0; k < amountOfJabs; k++)
                {
                    JabsThatOverlap[j, k] = model.NewBoolVar("JabsThatOverlap_" + j + "_" + k);
                }
            }

            // Add constraints on the first n jabs (the first jabs)
            for (int j = 0; j < amount; j++)
            {
                // The endtime of the Jab equals to the starttime + processing time from jab 1
                model.Add(EndTimeOfJab[j] == StartTimeOfJab[j] + (p1 - 1));
            }

            // Add constraints on the second n jabs (the first jabs)
            for (int j = amount; j < amountOfJabs; j++)
            {
                // Starttime of patient j - amount must be after the start of the second interval
                model.Add(StartTimeOfJab[j] >= EndTimeOfJab[j - amount] + gap + patients[j - amount].Delay);
                // The endtime of the jab equals to the starttime + processing time from jab 2
                model.Add(EndTimeOfJab[j] == StartTimeOfJab[j] + (p2 - 1));
                // Endtime of patient j - amount seconds jab must be before the end of the given first interval
                model.Add(EndTimeOfJab[j] <= EndTimeOfJab[j - amount] + gap + patients[j - amount].Delay + patients[j - amount].Interval);
            }

            for (int j = 0; j < amountOfJabs; j++)
            {
                for (int k = 0; k < amountOfJabs; k++)
                {
                    if (j == k)
                        continue;

                    //Set the overlap constraints
                    model.Add(StartTimeOfJab[j] - EndTimeOfJab[k] >= -maxTime * JabsThatOverlap[k, j]);
                    model.Add(StartTimeOfJab[j] - EndTimeOfJab[k] < maxTime * (1 - JabsThatOverlap[k, j]));

                    for (int i = 0; i < hospitalAmount; i++)
                    {
                        // Two jabs that overlap cannot be given in the same hospital
                        model.Add(JabInHospital[i, j] + JabInHospital[i, k] + JabsThatOverlap[j, k] + JabsThatOverlap[k, j] <= 3);
                    }
                }
            }

            for (int j = 0; j < amountOfJabs; j++)
            {
                // A jab is always assigned to exactly one hospital
                LinearExpr sum = null;
                for (int i = 0; i < hospitalAmount; i++)
                {
                    sum += JabInHospital[i, j];
                }
                model.Add(sum == 1);
            }

            for(int i = 0; i < hospitalAmount; i++)
            {
                // If one or more jabs are assigned to a hospital mark the hospital as used
                LinearExpr sum = null;
                for(int j = 0; j < amountOfJabs; j++)
                {
                    sum += JabInHospital[i, j];
                }
                model.Add(Hospitals[i] * amountOfJabs >= sum);
            }

            // Minimize the amount of used hospitals
            LinearExpr objective = null;
            for (int i = 0; i < hospitalAmount; i++)
            {
                objective += Hospitals[i];
            }
            
            model.Minimize(objective);

            solver = new CpSolver();
            solver.StringParameters = "num_search_workers:8";
            //solver.StringParameters = "log_search_progress:true";
            //solver.StringParameters = "max_time_in_seconds:10";
            sw.Start();
            var status = solver.Solve(model);
            sw.Stop();
            
            if (status == CpSolverStatus.Optimal)
            {
                if (logging)
                {
                    Console.WriteLine("Hospitals used: " + solver.ObjectiveValue);
                    for(int j = 0; j < amount; j++)
                    {
                        Console.WriteLine("Patient: {0} ", j + 1);
                        Console.WriteLine("     First Jab Start Time: {0}, Second Jab Start Time: {1}", solver.Value(StartTimeOfJab[j]), solver.Value(StartTimeOfJab[j + amount]));
                        Console.WriteLine("     First Jab End Time: {0}, Second Jab End Time: {1}", solver.Value(EndTimeOfJab[j]), solver.Value(EndTimeOfJab[j + amount]));
                        Console.WriteLine("     Possible first interval: [{0}, {1}], Second interval: [{2},{3}] (based on time)", 
                            patients[j].First, 
                            patients[j].Last, 
                            solver.Value(EndTimeOfJab[j]) + gap + patients[j].Delay, 
                            solver.Value(EndTimeOfJab[j]) + gap + patients[j].Delay + patients[j].Interval
                        );
                    }

                    for(int i = 0; i < hospitalAmount; i++)
                    {
                        Console.WriteLine("Hospital: " + i + " is used: " + solver.BooleanValue(Hospitals[i]));
                    }
                }
                // WritingToFile(sw.Elapsed.ToString() + " --- " + solver.ObjectiveValue);
            } else
            {
                Console.WriteLine(status);
            }
        }

        public void WritingToFile(string txt)
        {
            //Open the File
            StreamWriter file = new StreamWriter("..\\..\\..\\results.txt", true);
            file.WriteLine(filename + ": " + txt);
            //file.WriteLine(filename + ": " + sw.ElapsedMilliseconds);
            file.Close();
        }

        public void ReadingFromFile()
        {
            string line;

            // Read the file and display it line by line.  
            StreamReader file = new StreamReader(filename);

            line = file.ReadLine();
            p1 = int.Parse(line);
            if (logging)
            {
                Console.WriteLine(line);
            }
            
            line = file.ReadLine();
            p2 = int.Parse(line);
            if (logging)
            {
                Console.WriteLine(line);
            }

            line = file.ReadLine();
            gap = int.Parse(line);
            if (logging)
            {
                Console.WriteLine(line);
            }

            line = file.ReadLine();
            amount = int.Parse(line);
            if (logging)
            {
                Console.WriteLine(line);
            }

            patients = new Patient[amount];

            string[] line2;

            for(int i = 0; i < amount; i++)
            {
                line = file.ReadLine();
                if (logging)
                {
                    Console.WriteLine(line);
                }
                line2 = line.Split(',');

                Patient patient = new Patient(int.Parse(line2[0]), int.Parse(line2[1]), int.Parse(line2[2]), int.Parse(line2[3]));
                patients[i] = patient;
            }

            file.Close();
        }

        public void ReadingFromConsole()
        {
        }
    }
}
