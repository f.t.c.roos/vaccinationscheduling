﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;

namespace VaccinationScheduling
{
    struct Machine
    {
        public Machine(long index)
        {
            Index = index;
            Planning = new Dictionary<long, SolutionPatient>();
        }

        public long Index;
        public Dictionary<long, SolutionPatient> Planning;
    }

    struct SolutionPatient
    {
        public SolutionPatient(long firstDose, long firstMachine, long secondDose, long secondMachine)
        {
            FirstDose = firstDose;
            SecondDose = secondDose;
            FirstMachine = firstMachine;
            SecondMachine = secondMachine;
        }

        public long FirstDose;
        public long SecondDose;
        public long FirstMachine;
        public long SecondMachine;
    }

    class Online
    {
        // processingTimeOfFirstDose
        long p1;
        // processingTimeOfSecondDose
        long p2;
        // minimum gap between the first and the second dose
        long gap;

        List<SolutionPatient> solution;
        List<long> machines;

        //This list consist of a plan (list of bools) for each machine on index
        List<List<bool>> machineAvailability;

        //List<List<long>> availability = new List<List<long>>();

        long maximumTimeslot;
        long oldMaximumTimeslot;
        bool onlyReturnHospitals;
        bool logging;
        bool optimizeGap = true;

        //string filename = "..\\..\\..\\inputOnlineExample.txt";
        //string filename = "..\\..\\..\\inputOnlineTestInstance-2.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\20.txt";
        string filename = "..\\..\\..\\extra_online_testcases\\25.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\30.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\35.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\40.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\45.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\50.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\100.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\1000.txt";
        //string filename = "..\\..\\..\\extra_online_testcases\\10000.txt";

        Stopwatch sw = new Stopwatch();

        public Online(bool loggingToConsole, bool onlyReturnHospitalsForOffline)
        {
            onlyReturnHospitals = onlyReturnHospitalsForOffline;
            logging = loggingToConsole;

            solution = new List<SolutionPatient>();
            machines = new List<long>(0);
            machineAvailability = new List<List<bool>>();
            maximumTimeslot = 0;
            oldMaximumTimeslot = 0;

            if (!onlyReturnHospitals)
            {
                Console.WriteLine("Online program");
                long AmountOfRuns = 1;
                // We will take the average of 10 runtimes
                for (long i = 0; i < AmountOfRuns; i++)
                {
                    solution = new List<SolutionPatient>();
                    machines = new List<long>(0);
                    machineAvailability = new List<List<bool>>();
                    maximumTimeslot = 0;

                    ReadingFromFile();
                }
                long average = sw.ElapsedMilliseconds / AmountOfRuns;
                WritingToFile("Optimize Gap: " + optimizeGap + ", average: " + average.ToString() + ", machine count: " + machines.Count);
                Console.WriteLine("The end");
            } 
        }

        // first:       the first available time slot of the first dose
        // last:        the last available time slot of the first dose
        // delay:       the patient-dependent delay between the first and the second doses
        // interval:    the length of the second dose feasible interval
        public void Scheduling(long first, long last, long delay, long interval)
        {
            if (logging) { Console.WriteLine("M count 1: " + machines.Count); }

            //Add (new) available timeslot for each machine to cover the extended time slots
            oldMaximumTimeslot = Math.Max(maximumTimeslot, oldMaximumTimeslot);
            maximumTimeslot = last + delay + gap + interval + 1;
            if(maximumTimeslot > oldMaximumTimeslot)
            {
                List<bool> extension = Enumerable.Repeat(true, (int)(maximumTimeslot - oldMaximumTimeslot)).ToList();
                foreach(List<bool> list in machineAvailability)
                {
                    list.AddRange(extension);
                }
            }

            //Initialize an object and bool for the new patient
            SolutionPatient patient = new SolutionPatient();
            bool dosePlanned = false;

            //Initialize array to save most optimal gap so far, with corresponding machine & timeslot. [gap, machine, timeslot].
            long[] bestTimeslot = new long[3] { long.MaxValue, -1, -1 };

            //Iterate over each possible starting time slot in [last-p1+1, first]
            for(long i = last - p1 + 1; i >= first; i--) 
            {
                if(dosePlanned) { break;  } //Stop if an optimal time interval was found

                //Iterate over all machines
                for(long j = 0; j < machines.Count; j++)
                {
                    //Availability of machine j
                    List<bool> availability = machineAvailability[(int)j];
                    long bestgap = long.MaxValue;

                    //Check if [i, i+p1-1] is available, secondly determine gap with predecessor (or t=0)
                    for (long x = i + p1 - 1; x >= 0; x--)
                    {
                        if (!optimizeGap)
                        {
                            if (availability[(int)x] == false)
                            {
                                if (x < i) //Check if there are enough available time slots 
                                {
                                    bestgap = 0;
                                }
                                break;
                            }
                        }
                        if (availability[(int)x] == false)
                        { 
                            if( x < i) { bestgap = i - x - 1; }
                            break;
                        }
                        if (x == 0)
                        {
                            bestgap = i - x - 1;
                        }
                    }
                    
                    //Check if an optimal preceding gap (p1) is found
                    if (bestgap % p1 == 0 && bestgap != long.MaxValue)
                    {
                        bestTimeslot = new long[3] { 0, j, i };
                        patient.FirstDose = i; patient.FirstMachine = j;
                        dosePlanned = true;

                        break;
                    }

                    //Update bestimeslot if bestgap has improved
                    if (bestgap < bestTimeslot[0]) {
                        bestTimeslot = new long[3] { bestgap, j, i }; //Save best gap so far
                    }             
                }
            }

            //If no available time interval was found, add new machine
            if (bestTimeslot[1] == -1)
            {
                long numOfMachines = machines.Count; 
                machines.Add(numOfMachines); //Create new machine 
                List<bool> availability = Enumerable.Repeat(true, (int)Math.Max(maximumTimeslot, oldMaximumTimeslot)).ToList();
                
                //Choose time interval as late as possible, remove time interval from availability 
                for (long n = last - p1 + 1; n <= last; n++) 
                {
                    availability[(int)n] = false; 
                }

                machineAvailability.Add(availability);
                patient.FirstDose = last - p1 + 1; patient.FirstMachine = numOfMachines; 

            }
            //If available time interval was found, plan on besttimeslot
            else
            {
                long i = bestTimeslot[2];
                long m = bestTimeslot[1];

                List<bool> availability = machineAvailability[(int)m];
                
                //Remove availability on dose spot
                for (long n = i; n <= i + p1 - 1; n++)
                {
                    availability[(int)n] = false;
                }

                patient.FirstDose = i; patient.FirstMachine = m;
            }

            if (logging) { Console.WriteLine("First dose: " + patient.FirstDose + "--" + patient.FirstMachine); }

            //Plan second dose
            first = patient.FirstDose + p1 + gap + delay; //Start of feasible second interval: t_i,1 + p_1 + g + x_i
            last = first + interval - 1;                  //End of feasible second interval: t_i,1 + p_1 + g + x_i + l_i - 1 = first + l_i - 1
            dosePlanned = false;
            bestTimeslot = new long[3] { long.MaxValue, -1, -1 };

            //Iterate over each possible starting time slot in [last-p2+1, first]
            for (long i = last - p2 + 1; i >= first; i--)
            {
                if (dosePlanned) { break; } //Stop if an optimal sport was found

                //Iterate over all possible machines
                for (long j = 0; j < machines.Count; j++)
                {
                    //Availability of machine j
                    List<bool> availability = machineAvailability[(int)j];
                    long bestgap = long.MaxValue;

                    //Check if [i, i+p2-1] is available, secondly determine gap with predecessor (or t=0)
                    for (long x = i + p2 - 1; x >= 0; x--)
                    {
                        if (!optimizeGap)
                        {
                            if(availability[(int)x] == false)
                            {
                                if (x < i) //Check if there are enough available time slots 
                                {
                                    bestgap = 0;
                                }
                                break;
                            }
                        }
                        else {
                            if (availability[(int)x] == false)
                            {
                                if (x < i) { bestgap = i - x - 1; }
                                break;
                            }
                            if (x == 0)
                            {
                                bestgap = i - x - 1;
                            }
                        }
                    }

                    //Optimal gap, plan first dose.
                    if (bestgap % p2 == 0 && bestgap != long.MaxValue)
                    {
                        bestTimeslot = new long[3] { 0, j, i };
                        patient.SecondDose = i; patient.SecondMachine = j;
                        dosePlanned = true;

                        break;
                    }

                    //Update bestimeslot if bestgap has improved
                    if (bestgap < bestTimeslot[0])
                    {
                        bestTimeslot = new long[3] { bestgap, j, i }; 
                    }
                }
            }

            //If no available time interval was found, add new machine
            if (bestTimeslot[1] == -1)
            {
                long numOfMachines = machines.Count;
                machines.Add(numOfMachines); //Create new machine
                List<bool> availability = Enumerable.Repeat(true, (int)Math.Max(maximumTimeslot, oldMaximumTimeslot)).ToList();

                //Remove availability on dose spot
                for (long n = last - p2 - 1; n <= last; n++) 
                {
                    availability[(int)n] = false;
                }

                machineAvailability.Add(availability);
                patient.SecondDose = last - p2 - 1; patient.SecondMachine = numOfMachines; 

            }
            //Plan dose on best timeslot
            else
            {
                long i = bestTimeslot[2];
                long m = bestTimeslot[1];

                List<bool> availability = machineAvailability[(int)m];

                //Remove availability on dose spot
                for (long n = i; n <= i + p2 - 1; n++)
                {
                    availability[(int)n] = false;
                }

                patient.SecondDose = i; patient.SecondMachine = m; 
            }
            

            if (logging)
                Console.WriteLine("Second dose: " + patient.SecondDose + "--" + patient.SecondMachine);
            
            solution.Add(patient);
        }

        public void ReadingFromFile()
        {
            string line;
            string[] line2;

            // Read the file and display it line by line.
            StreamReader file = new StreamReader(filename); 

            line = file.ReadLine();
            p1 = long.Parse(line);
            if (logging) { Console.WriteLine(line); }

            line = file.ReadLine();
            p2 = long.Parse(line);
            if (logging) { Console.WriteLine(line); }

            line = file.ReadLine();
            gap = long.Parse(line);
            if (logging) { Console.WriteLine(line); }

            sw.Start();
            while ((line = file.ReadLine()) != "x")
            {
                if (logging) { Console.WriteLine(line); }
                line2 = line.Split(',');
                Scheduling(long.Parse(line2[0]), long.Parse(line2[1]), long.Parse(line2[2]), long.Parse(line2[3]));
            }
            sw.Stop();

            outputSolution(solution);
            //Console.WriteLine("machine count: " + machines.Count);
        }

        public void ReadingFromArray(long p1, long p2, long gap, Patient[] patients)
        {
            this.p1 = p1;
            this.p2 = p2;
            this.gap = gap;
            for (long i = 0; i < patients.Length; i++)
            {
                Scheduling(patients[i].First, patients[i].Last, patients[i].Delay, patients[i].Interval);
            }
        }

        public long outputMachines()
        {
            return machines.Count;
        }

        public void outputSolution(List<SolutionPatient> solution)
        {
            foreach(SolutionPatient patient in solution)
            {
                if (logging) { Console.WriteLine(patient.FirstDose + ", " + patient.FirstMachine + ", " + patient.SecondDose + ", " + patient.SecondMachine + ", "); }
                WritingToFile2(patient.FirstDose + ", " + patient.FirstMachine + ", " + patient.SecondDose + ", " + patient.SecondMachine + ", ");
            }
            if (logging) { Console.WriteLine("machine count: " + machines.Count); }
        }

        public void WritingToFile(string txt)
        {
            //Open the File
            StreamWriter file = new StreamWriter("..\\..\\..\\results.txt", true);
            file.WriteLine(filename + ": " + txt);
            //file.WriteLine(filename + ": " + sw.ElapsedMilliseconds);
            file.Close();
        }
        public void WritingToFile2(string txt)
        {
            //Open the File
            StreamWriter file = new StreamWriter("..\\..\\..\\solution.txt", true);
            file.WriteLine(txt);
            //file.WriteLine(filename + ": " + sw.ElapsedMilliseconds);
            file.Close();
        }

        public void ReadingFromConsole()
        {
        }
    }
}
